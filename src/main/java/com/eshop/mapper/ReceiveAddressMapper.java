package com.eshop.mapper;

import com.eshop.domain.ReceiveAddress;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ReceiveAddressMapper {
    @Select("SELECT * FROM `s_receiving_address` where user_id=#{userId}")
    List<ReceiveAddress> queryAddressByUserID(String userId);
    @Insert("insert into s_receiving_address " +
            "(id,receiving_address,receiving_person,mobile_phone,user_id,is_default) " +
            "values(#{id},#{receivingAddress},#{receivingPerson},#{mobilePhone},#{userId},#{isDefault})")
    int addAddress(ReceiveAddress receiveAddress);
    @Update("update s_receiving_address set " +
            "receiving_address=#{receivingAddress},receiving_person=#{receivingPerson}," +
            "mobile_phone=#{mobilePhone},user_id=#{userId},is_default=#{isDefault} " +
            "where id=#{id} "
    )
    int updateAddress(ReceiveAddress receiveAddress);
    @Update("update s_receiving_address set is_default=-1 where is_default=1 and user_id=#{userId} ")
    void updateAddressAll(@Param("userId") String userId);
    @Delete("delete from s_receiving_address where id=#{id}")
    int deleteAddressById(String id);
    @Update("update s_receiving_address set is_default=1 where id=#{id} ")
    int setDefault(String id);
}
