package com.eshop.mapper.provider;


import com.eshop.domain.Product;
import org.apache.ibatis.annotations.Param;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.util.StringUtils;

public class ProductProvider {

    public String queryProductPage(@Param("start") int start, @Param("pageSize") Integer pageSize,
                                   @Param("productName") final String productName,
                                   @Param("productTypeID") final String productTypeID){

        SQL sql= new SQL() {{
            SELECT("*");
            FROM(" s_product sp ");
            LEFT_OUTER_JOIN("s_product_type spe on sp.`product_type` = spe.`id` ");
            LEFT_OUTER_JOIN("s_brand sb on sp.product_brand = sb.id  ");
            if (!StringUtils.isEmpty(productName)){
                WHERE(" sp.product_name like  \"%\"#{productName}\"%\"");
            }
            if (!StringUtils.isEmpty(productTypeID)) {
                WHERE(" spe.id=#{productTypeID} ");
            }
            LIMIT("#{start}, #{pageSize}");
        }};
        return sql.toString();
    }

    public String queryProductCount( @Param("productName") final String productName,
                                     @Param("productTypeID") final String productTypeID){
        SQL sql= new SQL() {{
            SELECT("count(*)");
            FROM(" s_product sp ");
            LEFT_OUTER_JOIN("s_product_type spe on sp.`product_type` = spe.`id` ");
            LEFT_OUTER_JOIN("s_brand sb on sp.product_brand = sb.id  ");
            if (!StringUtils.isEmpty(productName)){
                WHERE(" sp.product_name like  \"%\"#{productName}\"%\"");
            }
            if (!StringUtils.isEmpty(productTypeID)) {
                WHERE(" spe.id= #{productTypeID} ");
            }

        }};
        return sql.toString();

    }
    public String queryProductsByType(@Param("product") final Product product, @Param("num") final int num){
        SQL sql=new SQL(){{
           SELECT("*");
           FROM("s_product sp ");
           LEFT_OUTER_JOIN(" s_product_type spt on sp.`product_type` = spt.`id` ");
           LEFT_OUTER_JOIN(" s_brand sb on  sp.product_brand = sb.id ");
           if (!StringUtils.isEmpty(product.getProductType().getId())){
               WHERE("spt.id=#{product.productType.id}");
           }
           if (!StringUtils.isEmpty(product.getProductType().getProductTypeName())){
                WHERE("spt.product_type_name=#{product.productType.productTypeName}");
           }
           if (!StringUtils.isEmpty(product.getProductName())){
               WHERE("sp.product_name like \"%\" #{product.productName} \"%\"");
           }
           if (product.getProductBrand()!=null&&!StringUtils.isEmpty(product.getProductBrand().getId())){
               String[] split = product.getProductBrand().getId().split(",");
               StringBuffer sb=new StringBuffer();
               sb.append("sb.id in (");
               for (int i = 0; i < split.length; i++) {
                   sb.append("'" + split[i] +"'");
                   if (i < split.length - 1) {
                       sb.append(",");
                   }
               }
               sb.append(")");
               WHERE(sb.toString());
           }
           if (num>0){
               LIMIT("0,#{num}");
           }

        }};

        return sql.toString();

    }







    /*
-- 查记录数
SELECT COUNT(sp.id) num
FROM `s_product` sp, `s_product_type` spe, s_brand sb
WHERE sp.`product_type` = spe.`id` AND sb.id = sp.product_brand

-- 查所有数据
SELECT
	-- 商品表
	sp.id,
	sp.product_name,
	sp.product_desc,
	sp.product_image,
	sp.price,
	sp.create_time,
	-- 商品品牌表
	sb.id AS brand_id,
	sb.brand_name,
	sb.brand_img,
	-- 商品类型表
	spe.id AS spe_id,
	spe.`product_type_name`,
	spe.`product_type_desc`
FROM
	s_product sp,
	s_product_type spe,
	s_brand sb

WHERE
	sp.`product_type` = spe.`id`
AND 	sp.product_brand = sb.id


*/
}
