package com.eshop.mapper;

import com.eshop.domain.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderMapper {
    @Select("select * from s_order where user_id = #{userId}")
    List<Order> queryAllOrder(String userId);
    @Insert("insert into s_order values(#{id},#{CreateTime},#{receiveAddress.id},#{userId})")
    void addOrderOne(Order order);
}
