package com.eshop.mapper;

import com.eshop.domain.Brand;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface BrandMapper {
    @Select("select * from s_brand b,s_product_type t where b.brand_type=t.id limit #{start},#{pageSize}")
    @Results(id ="brand_type" ,value = {

            @Result(column = "brand_type",property = "productType.id"),
            @Result(column = "product_type_name",property = "productType.productTypeName"),
            @Result(column = "product_type_desc",property = "productType.productTypeDesc"),
            @Result(column = "product_type_icon",property = "productType.productTypeIcon"),
    })
    List<Brand> queryBrandPages(@Param("start") int start, @Param("pageSize") Integer pageSize);
    @Select("select count(*) from s_brand")
    int queryBrandCount();
    @Insert("INSERT INTO `s_brand` (id, brand_name, brand_type,brand_img)VALUES (#{id},#{brandName},#{productType.id},#{brandImg})")
    int addBrand(Brand brand);
    @Select("select * from s_brand b,s_product_type t where b.brand_type=t.id and b.id=#{id};")
    @ResultMap("brand_type")
    Brand queryBrandById(@Param("id") String id);
    @Update("update s_brand set brand_name = #{brandName},brand_type=#{productType.id}, brand_img=#{brandImg} where id=#{id}")
    int updateBrand(Brand brand);
    @Delete("delete from s_brand where id=#{id}")
    int delete(@Param("id") String id);
    @Select("select * from s_brand")
    List<Brand> queryBrandAll();
    @Select("select * from s_brand b,s_product_type t where b.brand_type=t.id and b.brand_type=#{type};")
    List<Brand> queryBrandByTypeId(String type);
}
