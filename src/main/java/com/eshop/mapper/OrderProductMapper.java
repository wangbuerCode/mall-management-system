package com.eshop.mapper;

import com.eshop.domain.OrderProduct;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrderProductMapper {
    @Select("select * from " +
            "s_order_product sop " +
            "left join s_product sp on sop.product_id = sp.id " +
            "where sop.order_id = #{orderId}")
    @Results(
            {       @Result(column="product_id",property = "product.id"),
                    @Result(column="product_name",property = "product.productName"),
                    @Result(column="product_image",property = "product.productImage"),
                    @Result(column="price",property = "product.price"),
                    @Result(column="product_desc",property = "product.productDesc"),
            }
    )
    List<OrderProduct> queryOrderProByOrderId(String orderId);
    @Insert("insert into s_order_product " +
            "(id,order_id,product_id,product_num) " +
            "values(#{id},#{order.id},#{product.id},#{productNum})")
    void addOrdProOne(OrderProduct orderProduct);
}
