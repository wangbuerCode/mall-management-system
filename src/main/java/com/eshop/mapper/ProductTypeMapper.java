package com.eshop.mapper;

import com.eshop.domain.ProductType;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ProductTypeMapper {
    @Select("SELECT * FROM `s_product_type` limit #{start}, #{pageSize}")
    List<ProductType> queryProductTypePage(@Param("start") int start, @Param("pageSize") Integer pageSize);

    @Select("select count(*) from s_product_type")
    int queryProductTypeCount();
    @Insert("INSERT INTO `s_product_type` (id, product_type_name, product_type_desc,product_type_icon)VALUES (#{id},#{productTypeName},#{productTypeDesc},#{productTypeIcon})")
    int addProductType(ProductType productType);
    @Select("SELECT * FROM `s_product_type` where id =#{id}")
    ProductType queryProductTypeById(@Param("id") String id);
    @Update("update s_product_type set product_type_name=#{productTypeName},product_type_desc=#{productTypeDesc},product_type_icon=#{productTypeIcon} where id=#{id}")
    int updateProductType(ProductType productType);
    @Delete("delete from s_product_type where id=#{id}")
    int deleteProductTypeById(String id);
    @Select("SELECT * FROM `s_product_type`")
    List<ProductType> queryProductTypeAll();
}
