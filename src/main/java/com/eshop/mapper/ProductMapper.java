package com.eshop.mapper;

import com.eshop.domain.Product;
import com.eshop.mapper.provider.ProductProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ProductMapper {
    @SelectProvider(type = ProductProvider.class, method = "queryProductPage")
    @Results(id = "productMap",value = {

            @Result(column = "product_brand",property = "productBrand.id"),
            @Result(column = "brand_name",property = "productBrand.brandName"),
            @Result(column = "brand_img",property = "productBrand.brandImg"),

            @Result(column = "product_type",property = "productType.id"),
            @Result(column = "product_type_name",property = "productType.productTypeName"),
            @Result(column = "product_type_desc",property = "productType.productTypeDesc"),
            @Result(column = "product_type_icon", property = "productType.productTypeIcon")
    })
    List<Product> queryProductPage(@Param("start") int start, @Param("pageSize") Integer pageSize,
                                   @Param("productName") String productName,
                                   @Param("productTypeID") String productTypeID);



    @SelectProvider(type = ProductProvider.class, method = "queryProductCount")
    int queryProductCount(@Param("productName") final String productName,
                          @Param("productTypeID") final String productTypeID);
    @Insert("insert into s_product values(#{id},#{productName},#{productImage}," +
            "#{price},#{productType.id},#{productDesc}," +
            "#{createTime},#{productBrand.id})")
    int addProduct(Product product);
    @Select("select * from s_product sp left join s_product_type spe on sp.`product_type` = spe.`id`" +
            "left join s_brand sb on sp.product_brand = sb.id where sp.id=#{id}")
    @ResultMap("productMap")
    Product queryProductById(String id);
    @Update("update  s_product set product_name=#{productName},product_image=#{productImage},price=#{price}," +
            "product_type=#{productType.id},product_desc=#{productDesc},product_brand=#{productBrand.id}" +
            "where id=#{id}")
    int updateProduct(Product product);
    @Delete("delete from s_product where id=#{id}")
    int delete(String id);
    @Select("select *  " +
            "from s_product sp left join s_product_type spt on sp.`product_type` = spt.`id` " +
            "left join s_brand sb on  sp.product_brand = sb.id  " +
            "order by sp.create_time desc" +
            " limit 0,#{num}")
    @ResultMap("productMap")
    List<Product> queryNewProduct(int num);
    @SelectProvider(type = ProductProvider.class, method = "queryProductsByType")
    @ResultMap("productMap")
    List<Product> queryProductsByType(@Param("product") Product product, @Param("num") int num);
    @Select("select * , IFNULL(SUM(sop.product_num),0) num " +
            "from s_product sp " +
            "left join s_product_type spt on sp.`product_type` = spt.`id` " +
            "left join s_brand sb on  sp.product_brand = sb.id " +
            "LEFT JOIN s_order_product sop ON  sop.product_id=sp.id " +
            " GROUP BY sp.id ORDER BY num DESC  " +
            "LIMIT 0,6")
    @ResultMap("productMap")
    List<Product> queryProductRankings();
    @Select("select IFNULL(SUM(sop.product_num),0) " +
            "from s_product sp " +
            "left join s_order_product sop on sp.id=sop.product_id " +
            "where sp.id=#{id} " +
            "group by sp.id ")
    int querySalesByProductId(String id);
}
