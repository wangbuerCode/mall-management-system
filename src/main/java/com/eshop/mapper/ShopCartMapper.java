package com.eshop.mapper;

import com.eshop.domain.ShopCart;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface ShopCartMapper {
    @Select("select * from s_shop_cart where user_id =#{userId}")
    ShopCart queryShopCartByUserID(String userId);
    @Insert("insert into s_shop_cart (id,cart_id,user_id) " +
            "values(#{id},#{cartId},#{userId})")
    void addShopCart(ShopCart shopCart1);

    int deleteById(String id);
}
