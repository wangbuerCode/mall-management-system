package com.eshop.mapper;

import com.eshop.domain.Carousel;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CarouselMapper {

    @Select("SELECT id,url,sequence_num FROM `s_carousel` ORDER BY sequence_num  limit #{start}, #{pageSize}")
    @Results(@Result(column = "sequence_num",property = "sequenceNum"))
    List<Carousel> queryCarouselPages(@Param("start") int start, @Param("pageSize") Integer pageSize);
    @Select("SELECT count(id)  FROM `s_carousel`")
    int queryCarouselCount();

    @Insert("INSERT INTO `s_carousel` (id, url, sequence_num)VALUES (#{id},#{url},#{sequenceNum})")
    int addCarousel(Carousel carousel);
    @Select("SELECT id,url,sequence_num FROM `s_carousel` where id=#{id} ORDER BY sequence_num  ")
    @Results(@Result(column = "sequence_num",property = "sequenceNum"))
    Carousel queryCarouselById(String id);
    @Update("update s_carousel set sequence_num = #{sequenceNum},url=#{url} where id=#{id}")
    int updateCarousel(Carousel carousel);
    @Delete("delete from s_carousel where id=#{id}")
    int delete(@Param("id") String id);
    @Select("select * from s_carousel ORDER BY sequence_num ASC")
    List<Carousel> queryCarouselAll();
}
