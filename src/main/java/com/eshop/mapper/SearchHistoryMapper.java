package com.eshop.mapper;

import com.eshop.domain.SearchHistory;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SearchHistoryMapper {
    @Select("SELECT * FROM s_search_history order by num desc LIMIT 0, #{num}")
    List<SearchHistory> querySearchHistoryPages(int num);
    @Select("SELECT count(search_words) FROM s_search_history where search_words = #{words}")
    boolean isExist(String words);
    @Update("UPDATE s_search_history SET num = num + 1 WHERE search_words = #{words}")
    void updateSearchHistory(String words);
    @Insert("INSERT INTO s_search_history ( id, search_words, num, search_time) VALUES (#{id},#{searchWords},#{num},#{searchTime})")
    void addSearchHistory(SearchHistory searchHistory);
}
