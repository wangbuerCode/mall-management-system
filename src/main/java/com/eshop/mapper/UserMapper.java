package com.eshop.mapper;

import com.eshop.domain.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {
    @Select("select * from s_user limit #{start},#{pageSize}")
    List<User> queryUserList(@Param("start") int start, @Param("pageSize") int pageSize);
    @Select("select count(*) from s_user")
    int queryUserCount();
    @Insert("insert into s_user(id,username,password,type) value(#{id},#{username},#{password},#{type})")
    int addUser(User user);
    @Select("select * from s_user where id=#{id}")
    User queryUserById(@Param("id") String id);
    @Update("update s_user set username = #{username},password=#{password} where id=#{id}")
    int updateUser(User user);
    @Delete("delete from s_user where id=#{id}")
    int deleteUser(String id);
    @Select("select * from s_user where username=#{username} and type=#{type}")
    User queryUserByName(@Param("username") String username, @Param("type") int type);
}
