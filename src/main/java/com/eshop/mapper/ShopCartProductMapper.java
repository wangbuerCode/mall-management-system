package com.eshop.mapper;

import com.eshop.domain.Product;
import com.eshop.domain.ShopCart;
import com.eshop.domain.ShopCartProduct;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ShopCartProductMapper {
    @Select("select * from " +
            "s_shop_cart_product sscp,s_product sp " +
            "where " +
            "sscp.product_id=sp.id " +
            "and sscp.shop_cart_id=#{cartId} ")
    @Results(
            {       @Result(column="product_id",property = "product.id"),
                    @Result(column="product_name",property = "product.productName"),
                    @Result(column="product_image",property = "product.productImage"),
                    @Result(column="price",property = "product.price"),
                    @Result(column="product_desc",property = "product.productDesc"),
            }
    )
    List<ShopCartProduct> queryCartProductAll(String cartId);
    @Insert("insert into s_shop_cart_product(id,shop_cart_id,product_id,product_num)" +
            "values(#{id},#{shopCart.id},#{product.id},#{productNum})")
    void addShop(ShopCartProduct shopCartProduct);
    @Delete("delete from s_shop_cart_product where id=#{id}")
    int deleteById(String id);
    @Select("select count(*) from s_shop_cart_product where product_id=#{id}")
    int queryShopCartByProductID(String id);
    @Update("update s_shop_cart_product set " +
            "product_num = product_num + #{productNum} " +
            "where product_id=#{product.id} and " +
            "shop_cart_id=#{shopCart.id}")
    void update(ShopCartProduct shopCartProduct);
    @Delete("delete from s_shop_cart_product " +
            "where shop_cart_id=#{cartId} and product_id= #{proId}")
    void deleteShopCartBy(@Param("cartId") String cartId, @Param("proId") String proId);
}
