package com.eshop.controller.front;

import com.eshop.domain.User;
import com.eshop.service.UserService;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/front/register")
public class FrontRegisterController {
    @Autowired
    private UserService userService;
    @RequestMapping("/registerPage")
    public String registerPage(){
        return "front/register";
    }
    @RequestMapping("/register")
    @ResponseBody
    public String  register(String username, String password, String confirmPassword, String code, HttpSession session){
      try {
          if (StringUtils.isEmpty(username)){
              return "用户名为空";
          }
          if (StringUtils.isEmpty(password)){
              return "密码为空";
          }
          if (StringUtils.isEmpty(confirmPassword)){
              return "确认密码为空";
          }
          if (!confirmPassword.equals(password)){
              return "密码出入不一致";
          }

          String code1=(String)session.getAttribute("code");
          if (StringUtils.isEmpty(code)&&!code.equals(code1)){
              return "验证码不能为空";
          }
          User user = userService.queryUserByName(username, 1);
          if (user == null){
              User user1=new User(UUIDUtils.getId(),username,password,1,null);
              userService.addUser(user1);
              return "注册成功";
          }else {
              return "用户名已存在";
          }
      }catch (Exception e){
          e.printStackTrace();
          return "服务器繁忙,注册失败";
      }
    }
}
