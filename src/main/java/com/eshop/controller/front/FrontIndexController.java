package com.eshop.controller.front;

import com.eshop.domain.Carousel;
import com.eshop.domain.Product;
import com.eshop.domain.ProductType;
import com.eshop.domain.SearchHistory;
import com.eshop.service.CarouselService;
import com.eshop.service.ProductService;
import com.eshop.service.ProductTypeService;
import com.eshop.service.SearchHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/front")
public class FrontIndexController {

    @Autowired//轮播图
    private CarouselService carouselService ;
    @Autowired//商品类型
    private ProductTypeService productTypeService ;
    @Autowired//商品
    private ProductService productService ;
    @Autowired
    private SearchHistoryService searchHistoryService;
    @RequestMapping("/index")
    public String index(Model model){
        //轮播图
        List<Carousel> carousels = carouselService.queryCarouselAll();
        model.addAttribute("allcarouselFigures",carousels);
        //分类
        List<ProductType> productTypes = productTypeService.queryProductTypeAll();
        model.addAttribute("allProductTypes",productTypes);
        //新品
        List<Product> newProducts = productService.queryNewProduct(6);
        model.addAttribute("newProducts", newProducts);
        //查询热搜词
        List<SearchHistory> searchHistorys = searchHistoryService.querySearchHistoryPages(10);
        model.addAttribute("searchHistorys",searchHistorys);
        //排行榜
        List<Product> rankings = productService.queryProductRankings();
        model.addAttribute("rankings", rankings);
        //全球进口
        ProductType productType = new ProductType();
        productType.setProductTypeName("全球进口");
        Product product = new Product();
        product.setProductType(productType);
        List<Product> list = productService.queryProductsByType(product, 5);
        model.addAttribute("list", list);
        //服装服饰
        productType.setProductTypeName("服装服饰");
        product.setProductType(productType);
        product.getProductType().setProductTypeName("服装服饰");
        List<Product> list2 = productService.queryProductsByType(product, 12);
        model.addAttribute("list2", list2);
        //护肤美妆
        productType.setProductTypeName("护肤美妆");
        product.setProductType(productType);
        List<Product> list3 = productService.queryProductsByType(product, 5);
        model.addAttribute("list3", list3);
        //图书学习
        productType.setProductTypeName("图书学习");
        product.setProductType(productType);
        List<Product> list4 = productService.queryProductsByType(product, 12);
        model.addAttribute("list4", list4);
        return "front/index/index";
    }
}
