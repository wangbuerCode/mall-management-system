package com.eshop.controller.front;

import com.eshop.domain.User;
import com.eshop.service.UserService;
import com.eshop.utils.EncryptionUtils;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/front/login")
public class FrontLoginController {
    @Autowired
    private UserService userService;
    @RequestMapping("/loginPage")
    public String loginPage(){
        return "front/login";
    }
    @RequestMapping("/login")
    @ResponseBody
    public String login(String username, String password, String code, String autoLogin,
                        HttpServletRequest request, HttpSession session, HttpServletResponse response){
        String code1 = (String) session.getAttribute("code");
        if(StringUtils.isNullOrEmpty(code)||!code.equalsIgnoreCase(code1)){
            return "验证码错误";
        }
        if(!StringUtils.isNullOrEmpty(username)){
            User user = userService.queryUserByName(username, 1);
            if(user==null){
                return "用户名不存在";
            }else {
                String psw1 = user.getPassword();
                if(psw1.equals(EncryptionUtils.encryptMD5(password))){
                    System.out.println("登录成功");
                    session.setAttribute("user",user);
                    if(!StringUtils.isNullOrEmpty(autoLogin)&&autoLogin.equals("1")){
                        Cookie username_cookie = new Cookie("username", username);
                        username_cookie.setMaxAge(3600*24*7);
                        username_cookie.setPath(request.getContextPath());
                        response.addCookie(username_cookie);
                    }else {
                        Cookie username_cookie = new Cookie("username", username);
                        username_cookie.setMaxAge(0);
                        username_cookie.setPath(request.getContextPath());
                        response.addCookie(username_cookie);
                    }
                    return "登录成功";
                }else {
                    return "密码错误";
                }
            }
        }else {
            return "用户名为空";
        }
    }
    @RequestMapping("/logout")
    public void logout(HttpSession session,HttpServletRequest request,HttpServletResponse response) throws IOException {
        session.removeAttribute("user");
        Cookie[] cookies = request.getCookies();
        if(cookies!=null){
            for (Cookie cookie : cookies) {
                if ("username".equals(cookie.getName())) {
                    cookie.setMaxAge(0);
                    cookie.setPath(request.getContextPath());
                    response.addCookie(cookie);
                }
            }
        }
        response.sendRedirect(request.getContextPath()+"/front/login/loginPage");

    }
}

