package com.eshop.controller.front;

import com.eshop.domain.ReceiveAddress;
import com.eshop.domain.User;
import com.eshop.service.ReceiveAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/front/receiveing_address")
public class ReceiveAddressController {
    @Autowired
    private ReceiveAddressService receiveAddressService;

    @RequestMapping("/index")
    public String index(HttpSession session, Model model){
        User user =(User) session.getAttribute("user");
        if (user ==null){
            return "redirect:/front/login/loginPage";
        }else {
            List<ReceiveAddress> receiveAddresses=receiveAddressService.queryAddressByUserID(user.getId());
            model.addAttribute("list",receiveAddresses);
        }
        return "front/receiving_address/deliverAddress";
    }
    @RequestMapping("save")
    @ResponseBody
    public String save(ReceiveAddress receiveAddress,HttpSession session, Model model) {
        try {
            User user = (User) session.getAttribute("user");
            List<ReceiveAddress> receiveAddresses = receiveAddressService.queryAddressByUserID(user.getId());
            receiveAddress.setUserId(user.getId());
            if (StringUtils.isEmpty(receiveAddress.getIsDefault()) || receiveAddress.getIsDefault() != 1) {
                receiveAddress.setIsDefault(-1);
            } else {
                receiveAddress.setIsDefault(1);
            }
            if (receiveAddresses.size() < 5 && StringUtils.isEmpty(receiveAddress.getId())) {
                    receiveAddressService.addAddress(receiveAddress);
                    return "保存成功";
            }else if(!StringUtils.isEmpty(receiveAddress.getId())){
                receiveAddressService.updateAddress(receiveAddress);
                return "保存成功";
            } else {
                throw new Exception(":地址储存达到上限请删除后一个后再添加");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "保存失败" + e.getMessage();

        }


    }
    @RequestMapping("delete")
    @ResponseBody
    public String delete(String id){

        try {
            int i = receiveAddressService.deleteAddressById(id);
            if (i==0){
                return "删除成功";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "删除失败"+e.getMessage();
        }
        return "删除成功";
    }
    @RequestMapping("/setDefault")
    @ResponseBody
    public String setDefault(String id,HttpSession session){
        try {
            User user = (User) session.getAttribute("user");
            int i = receiveAddressService.setDefault(id,user.getId());
            if (i==0){
                return "设置成功";
            }
        }catch (Exception e){
            e.printStackTrace();
            return "修改失败"+e.getMessage();
        }
        return "设置成功";
    }
}

