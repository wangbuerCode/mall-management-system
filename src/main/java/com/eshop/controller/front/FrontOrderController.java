package com.eshop.controller.front;

import com.eshop.domain.Order;
import com.eshop.domain.OrderProduct;
import com.eshop.domain.User;
import com.eshop.service.OrderProductService;
import com.eshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("front/order")
public class FrontOrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderProductService orderProductService;
    @RequestMapping("index")
    private String index(HttpSession session, Model model){
        try {
            User user = (User) session.getAttribute("user");
            if (user == null) {
                return "redirect:/front/login/loginPage";
            } else {
                List<Order> list = orderService.queryAllOrder(user.getId());
                for (Order order : list) {
                    List<OrderProduct> orderProducts = orderProductService.queryOrderProByOrderId(order.getId());
                    order.setList(orderProducts);
                }
                model.addAttribute("list", list);
                return "front/order/order";
            }
        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("errMessage","服务器繁忙"+e.getMessage());
            return "500";
        }
    }
}
