package com.eshop.controller.front;

import com.alibaba.fastjson.JSON;
import com.eshop.domain.*;
import com.eshop.service.*;
import com.eshop.utils.UUIDUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("front/shop_cart")
public class ShopCartController {
    @Autowired
    private ReceiveAddressService receiveAddressService;
    @Autowired
    private ShopCartProductService shopCartProductService;
    @Autowired
    private ShopCartService shopCartService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderProductService orderProductService;

    @RequestMapping("/shopCart")
    public String index(HttpSession session, Model model){
        try{
            User user =(User) session.getAttribute("user");
            if (user ==null){
                return "redirect:/front/login/loginPage";
            }else {
                ShopCart shopCart=shopCartService.queryShopCartByUserID(user.getId());
                List<ReceiveAddress> receiveAddresses=receiveAddressService.queryAddressByUserID(user.getId());
                model.addAttribute("address",receiveAddresses);
                List<ShopCartProduct> list=shopCartProductService.queryCartProductAll(shopCart.getId());
                model.addAttribute("list",list);
            }
            return "front/shop_cart/shop_cart";

        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("errMessage","服务器繁忙"+e.getMessage());
            return "500";
        }

    }
    @RequestMapping("addProductToCart")
    @ResponseBody
    public String addProductToCart(HttpSession session, String product_id,Integer product_num) throws JsonProcessingException {
        Map map =new HashMap();
        try{
            User user =(User) session.getAttribute("user");
            if (user ==null){
                map.put("message","请登录后再操作");
                map.put("url","/front/login/loginPage");
            }else {
                ShopCart shopCart=shopCartService.queryShopCartByUserID(user.getId());
                ShopCartProduct shopCartProduct=new ShopCartProduct();
                Product product=new Product();
                product.setId(product_id);
                shopCartProduct.setProduct(product);
                shopCartProduct.setShopCart(shopCart);
                shopCartProduct.setProductNum(product_num);
                shopCartProductService.addShop(shopCartProduct);
                map.put("result",true);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("message","添加失败"+e.getMessage());
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String val = objectMapper.writeValueAsString(map);
        return val;
    }
    @RequestMapping("/deleteProduct")
    @ResponseBody
    public String delete(String id) throws JsonProcessingException {
        Map map =new HashMap();
        try {
            int i = shopCartProductService.deleteById(id);
            if (i==0){
                map.put("message","删除失败");
            }else {
                map.put("message","删除成功");
                map.put("result",true);
            }
        }catch (Exception e){
            e.printStackTrace();;
            map.put("message","删除失败:"+e.getMessage());
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String val = objectMapper.writeValueAsString(map);
        System.out.println(val);
        return val;
    }
    @RequestMapping("/batchDel")
    @ResponseBody
    public String batchDel(String[] ids) throws JsonProcessingException {
        Map map =new HashMap();
        try {
            shopCartProductService.deleteAll(ids);
            map.put("message","删除成功");
            map.put("result",true);
        }catch (Exception e){
            e.printStackTrace();;
            map.put("message","删除失败:"+e.getMessage());
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String val = objectMapper.writeValueAsString(map);
        System.out.println(val);
        return val;
    }
    @RequestMapping("compute")
    @ResponseBody
    public String compute(String products,String address_id,HttpSession session) throws JsonProcessingException {
        Map map =new HashMap();
        User user =(User) session.getAttribute("user");
        try {
            if (user ==null){
                map.put("message","请登录后再操作");
                map.put("url","/front/login/loginPage");
            }else {
                List<ProIdAndNum> proIdAndNums= JSON.parseArray(products,ProIdAndNum.class);
                Order order=new Order();
                String OrderProId=UUIDUtils.getId();
                order.setId(OrderProId);
                order.setCreateTime(new Date());
                order.setUserId(user.getId());
                ReceiveAddress receiveAddress = new ReceiveAddress();
                receiveAddress.setId(address_id);
                order.setReceiveAddress(receiveAddress);
                orderService.addOrderOne(order);
                ShopCart shopCart=shopCartService.queryShopCartByUserID(user.getId());
                for (ProIdAndNum proIdAndNum : proIdAndNums) {
                    OrderProduct orderProduct=new OrderProduct();
                    orderProduct.setId(UUIDUtils.getId());
                    orderProduct.setOrder(order);
                    Product product=new Product();
                    product.setId(proIdAndNum.getId());
                    orderProduct.setProduct(product);
                    orderProduct.setProductNum(proIdAndNum.getNum());
                    orderProductService.addOrdProOne(orderProduct);
                    shopCartProductService.deleteShopCartBy(shopCart.getId(),proIdAndNum.getId());
                }
                map.put("message","结算成功");
                map.put("result",true);
            }
        }catch (Exception e){
            map.put("message","服务器繁忙:"+e.getMessage());
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String val = objectMapper.writeValueAsString(map);
        System.out.println(val);
        return val;
    }

}
