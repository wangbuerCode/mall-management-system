package com.eshop.controller.front;

import com.eshop.domain.Brand;
import com.eshop.domain.Product;
import com.eshop.domain.ProductType;
import com.eshop.domain.SearchHistory;
import com.eshop.service.BrandService;
import com.eshop.service.ProductService;
import com.eshop.service.ProductTypeService;
import com.eshop.service.SearchHistoryService;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/front/productType")
public class FrontTypeController {
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SearchHistoryService searchHistoryService;
    @RequestMapping("index")
    public String index(@RequestParam(defaultValue = "") String type,
                        @RequestParam(defaultValue = "") String words,
                        String brands,
                        Model model){
        //所有商品分类
        List<ProductType> allProductTypes = productTypeService.queryProductTypeAll();
        //类别
       /* ProductType productType = productTypeService.queryProductTypeById(type);*/
        //品牌
        List<Brand> listBrand = brandService.queryBrandByTypeId(type);
        Product product = new Product();
        product.setProductName(words);
        Brand brand=new Brand();
        brand.setId(brands);
        ProductType productType=new ProductType();
        productType.setId(type);
        product.setProductType(productType);
        product.setProductBrand(brand);
        //商品
        List<Product>  datas= productService.queryProductsByType(product, 0);
        for (Product data : datas) {
            int sales = productService.querySalesByProductId(data.getId());
            data.setSales(sales);
        }
        List<SearchHistory> searchHistories = searchHistoryService.querySearchHistoryPages(10);
        model.addAttribute("allProductTypes",allProductTypes);

        model.addAttribute("searchHistorys",searchHistories);
        model.addAttribute("listBrand",listBrand);
        model.addAttribute("datas",datas);
        model.addAttribute("type",type);
        if (!StringUtils.isEmpty(words)){
            searchHistoryService.addUpdateWords(words);
        }


        return "front/product_type/product_type";
    }
}
