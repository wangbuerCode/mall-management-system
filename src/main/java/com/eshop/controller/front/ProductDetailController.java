package com.eshop.controller.front;

import com.eshop.domain.Product;
import com.eshop.domain.ProductType;
import com.eshop.domain.SearchHistory;
import com.eshop.service.BrandService;
import com.eshop.service.ProductService;
import com.eshop.service.ProductTypeService;
import com.eshop.service.SearchHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("front/product_detail")
public class ProductDetailController {
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SearchHistoryService searchHistoryService;
    @RequestMapping("productDetail")
    private String productDetail(String id, Model model){
        //所有商品分类
        List<ProductType> allProductTypes = productTypeService.queryProductTypeAll();
        model.addAttribute("allProductTypes",allProductTypes);
        List<SearchHistory> searchHistories = searchHistoryService.querySearchHistoryPages(10);
        model.addAttribute("searchHistorys",searchHistories);
        Product product = productService.queryProductById(id);
        int sales = productService.querySalesByProductId(product.getId());
        product.setSales(sales);
        model.addAttribute("product",product);
        ProductType productType = product.getProductType();
        ProductType productType1=new ProductType();
        Product product1=new Product();
        productType1.setId(productType.getId());
        product1.setProductType(productType1);
        List<Product> products = productService.queryProductsByType(product1, 0);
        List<Product> list=new ArrayList<Product>();

        for (int i = 0 ,j=2; i < products.size() && i<j; i++) {
            if (!products.get(i).getId().equals(product.getId())){
                int sales1 = productService.querySalesByProductId(products.get(i).getId());
                products.get(i);
                list.add(products.get(i));
            }else {
                j++;
            }
        }

        model.addAttribute("list",list);
        return "front/product_detail/product_detail";
    }
}
