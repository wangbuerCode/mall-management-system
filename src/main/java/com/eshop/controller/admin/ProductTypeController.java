package com.eshop.controller.admin;

import com.eshop.domain.Carousel;
import com.eshop.domain.ProductType;
import com.eshop.service.ProductTypeService;
import com.eshop.utils.IconfontUtils;
import com.eshop.utils.PageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/admin/productType")
public class ProductTypeController {
    @Autowired
    private ProductTypeService service;

    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1") Integer pageNo,
                       @RequestParam(defaultValue = "5") Integer pageSize,
                       Model model) {

        try{
            PageModel<ProductType> productTypePages=service.queryProductTypePage(pageNo,pageSize);
            model.addAttribute("productTypePages",productTypePages);
        } catch (Exception e) {
        e.printStackTrace();
        model.addAttribute("errMessage", "查询失败："+e.getMessage());
        return  "500";
         }
       return "admin/product_type/list";

    }
    @RequestMapping("/addPage")
    public String addPage(HttpServletRequest request,Model model) throws IOException {
        List<String> iconfonts = IconfontUtils.getIconfonts(request);
        model.addAttribute("iconfonts",iconfonts);
        return "admin/product_type/add";
    }
    @RequestMapping("/add")
    public String add(ProductType productType, Model model){
        try {
            int i = service.addProductType(productType);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/productType/list");
        return "success";
    }
    @RequestMapping("updatePage")
    public String updatePage(HttpServletRequest request,Model model,String id) throws IOException {
        ProductType productType= service.queryProductTypeById(id);
        List<String> iconfonts = IconfontUtils.getIconfonts(request);
        model.addAttribute("iconfonts",iconfonts);
        model.addAttribute("productType",productType);
        return "admin/product_type/update";
    }
    @RequestMapping("/update")
    public String update(ProductType productType, Model model){
        try {
            int i = service.updateProductType(productType);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/productType/list");
        return "success";
    }
    @RequestMapping("deletePage")
    public String deletePage(String id,Model model) throws IOException {
        model.addAttribute("id",id);
        return "admin/product_type/delete";
    }
    @RequestMapping("/delete")
    public String delete(String id, Model model){
        try {
            int i = service.deleteProductTypeById(id);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/productType/list");
        return "success";
    }
    @RequestMapping("batchDel")
    @ResponseBody
    public String batchDel(String[] ids) {
        System.out.println("进来了");
        service.batchProductTypeDel(ids);
        return "/admin/productType/list";

    }
}
