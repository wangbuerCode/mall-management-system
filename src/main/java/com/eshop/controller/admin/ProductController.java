package com.eshop.controller.admin;

import com.eshop.domain.Brand;
import com.eshop.domain.Product;
import com.eshop.domain.ProductType;
import com.eshop.service.BrandService;
import com.eshop.service.ProductService;
import com.eshop.service.ProductTypeService;
import com.eshop.utils.PageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("admin/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private BrandService brandService;
    @RequestMapping("/list")
    public String list( @RequestParam(defaultValue = "1") Integer pageNo,
                        @RequestParam(defaultValue = "5")Integer pageSize,
                        String productName,
                        String productTypeID,
                        Model model){
        try {
            List<ProductType> productTypes = productTypeService.queryProductTypeAll();
            model.addAttribute("productTypes", productTypes);
            model.addAttribute("productName",productName);
            model.addAttribute("type",productTypeID);
            PageModel<Product> productPages= productService.queryProductPage(pageNo,pageSize,productName,productTypeID);
            model.addAttribute("productPages",productPages);
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errMessage", "查询失败："+e.getMessage());
            return  "500";
        }
        return "admin/product/list";
    }
    @RequestMapping("addPage")
    public String addPage(Model model){
        List<ProductType> productTypes= productTypeService.queryProductTypeAll();
        List<Brand> brands=  brandService.queryBrandAll();
        model.addAttribute("productTypes",productTypes);
        model.addAttribute("brands",brands);
        return "admin/product/add";
    }
    @RequestMapping("add")
    private String addProduct(Product product,Model model){
        try {
            int i = productService.addProduct(product);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/product/list");
        return "success";


    }
    @RequestMapping("updatePage")
    public String update(String id,Model model){
        Product product= productService.queryProductById(id);
        List<ProductType> productTypes= productTypeService.queryProductTypeAll();
        List<Brand> brands=  brandService.queryBrandAll();
        model.addAttribute("productTypes",productTypes);
        model.addAttribute("brands",brands);
        model.addAttribute("product",product);
        return "admin/product/update";
    }
    @RequestMapping("update")
    private String update(Product product,Model model){
        try {
            int i = productService.updateProduct(product);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/product/list");
        return "success";


    }
    @RequestMapping("deletePage")
    public String deletePage(String id,Model model){
        model.addAttribute("id",id);
        return "admin/product/delete";
    }
    @RequestMapping("/delete")
    public String delete(String id, Model model){
        try {
            int i = productService.delete(id);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/product/list");
        return "success";

    }
    @RequestMapping("batchDel")
    @ResponseBody
    public String batchDel(String[] ids) {
        System.out.println("进来了");
        productService.batchProductTypeDel(ids);
        return "/admin/product/list";

    }

}
