package com.eshop.controller.admin;

import com.eshop.domain.Brand;
import com.eshop.domain.Carousel;
import com.eshop.domain.ProductType;
import com.eshop.service.BrandService;
import com.eshop.service.ProductTypeService;
import com.eshop.utils.PageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.naming.Name;
import java.util.List;

@Controller
@RequestMapping("/admin/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductTypeService productTypeService;
    @RequestMapping("/list")
    public String list( @RequestParam(defaultValue = "1") Integer pageNo,
                       @RequestParam(defaultValue = "5")Integer pageSize,
                       Model model){
        try {
            PageModel<Brand> brandPages = brandService.queryBrandPages(pageNo, pageSize);
            model.addAttribute("brandPages", brandPages);
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errMessage", "查询失败："+e.getMessage());
            return  "500";
        }
        return "admin/brand/list";
    }
    @RequestMapping("addPage")
    public String addPage(Model model){
        List<ProductType> productTypes= productTypeService.queryProductTypeAll();
        model.addAttribute("productTypes",productTypes);
        return "admin/brand/add";
    }
    @RequestMapping("/add")
    private String addBrand(Brand brand,Model model){
        try {
            int i = brandService.addBrand(brand);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/brand/list");
        return "success";


    }
    @RequestMapping("updatePage")
    public String updatePage(String id,Model model){
       Brand brand= brandService.queryBrandById(id);
        List<ProductType> productTypes= productTypeService.queryProductTypeAll();
        model.addAttribute("productTypes",productTypes);
        model.addAttribute("brand",brand);
        return "admin/brand/update";
    }
    @RequestMapping("/update")
    private String update(Brand brand,Model model){
        try {
            int i = brandService.updateBrand(brand);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/brand/list");
        return "success";


    }
    @RequestMapping("deletePage")
    public String deletePage(String id,Model model){
        model.addAttribute("id",id);
        return "admin/carousel/delete";
    }
    @RequestMapping("/delete")
    public String delete(String id, Model model){
        try {
            int i = brandService.delete(id);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/brand/list");
        return "success";

    }
    @RequestMapping("batchDel")
    @ResponseBody
    public String batchDel(String[] ids) {
        System.out.println("进来了");
        brandService.batchProductTypeDel(ids);
        return "/admin/brand/list";

    }
}
