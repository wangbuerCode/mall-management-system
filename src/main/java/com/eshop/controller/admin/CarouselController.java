package com.eshop.controller.admin;

import com.eshop.domain.Carousel;
import com.eshop.service.CarouselService;
import com.eshop.utils.PageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin/carousel")
public class CarouselController {
    @Autowired
    private CarouselService service;
    @RequestMapping("/list")
    public String list(@RequestParam(defaultValue = "1") Integer pageNo,
                       @RequestParam(defaultValue = "5")Integer pageSize,
                       Model model){
        try {
            PageModel<Carousel> CarouselPages = service.queryCarouselPages(pageNo, pageSize);
            model.addAttribute("CarouselPages", CarouselPages);
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("errMessage", "查询失败："+e.getMessage());
            return  "500";
        }
        return "admin/carousel/list";
    }

    @RequestMapping("addPage")
    public String addPage(){
        return "admin/carousel/add";
    }
    @RequestMapping("/add")
    public String add(Carousel carousel, Model model){
        try {
            int i = service.addCarousel(carousel);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/carousel/list");
        return "success";

    }
    @RequestMapping("updatePage")
    public String updatePage(String id,Model model){
        Carousel carousel=service.queryCarouselById(id);
        model.addAttribute("carousel",carousel);
        return "admin/carousel/update";
    }
    @RequestMapping("/update")
    public String update(Carousel carousel, Model model){
        try {
            int i = service.updateCarousel(carousel);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/carousel/list");
        return "success";

    }

    @RequestMapping("deletePage")
    public String deletePage(String id,Model model){
        model.addAttribute("id",id);
        return "admin/carousel/delete";
    }
    @RequestMapping("/delete")
    public String delete(String id, Model model){
        try {
            int i = service.delete(id);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙操作失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/carousel/list");
        return "success";

    }

}
