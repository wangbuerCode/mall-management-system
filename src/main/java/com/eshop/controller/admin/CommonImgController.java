package com.eshop.controller.admin;


import com.eshop.utils.CodeUtil;
import com.eshop.utils.ImgPathConfig;
import com.eshop.utils.UUIDUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/common")
public class CommonImgController {
    @Autowired
    private ImgPathConfig imgPathConfig;
    @RequestMapping("/getImage")
    public void getImage(String image, HttpServletResponse response) {
        try {
            File file = new File(imgPathConfig.getImgPath() , image);

            if (file.exists()) {
                ServletOutputStream os = response.getOutputStream();
                FileInputStream is = new FileInputStream(file);
                //边读边写
                IOUtils.copy(is,os);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping("/upload")
    @ResponseBody
    public String upload(@RequestParam(name = "file") MultipartFile mf) throws JsonProcessingException {
        String path = imgPathConfig.getImgPath();
        String imgName = mf.getOriginalFilename();
        SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd");
        String date = df2.format(new Date());
        imgName = UUIDUtils.getId() + date+".jpg";
        System.out.println(imgName);

        try {
            File file = new File(path,imgName);
            mf.transferTo(file);//上传
        } catch (Exception e) {
            e.printStackTrace();
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String s = objectMapper.writeValueAsString(imgName);
        return s;
    }
    @RequestMapping("/code")
    @ResponseBody
    public void code(HttpServletResponse response, HttpSession session){
        try {
            //1.将一个图片文件写进指定的输出流中
            String codeText = CodeUtil.generateCodeAndPic(response.getOutputStream());
            //2.将验证码文本存进session中
            session.setAttribute("code",codeText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
