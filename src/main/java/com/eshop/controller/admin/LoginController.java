package com.eshop.controller.admin;

import com.eshop.domain.User;
import com.eshop.service.UserService;
import com.eshop.utils.EncryptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class LoginController {
    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public String index(){
        return "admin/index/index";
    }
    @RequestMapping("/loginPage")
    public String loginPage(){
        return "admin/login/login";
    }
    @RequestMapping("/login")
    public String login(String username, String password, Model model, HttpSession session){
        try {
            User user= userService.queryUserByName(username,0);
            if (user==null){
                model.addAttribute("errMsg","用户名不存在");
                return "admin/login/login";
            }else {
                String password1 = user.getPassword();
                if (!EncryptionUtils.encryptMD5(password).equals(password1)){
                    model.addAttribute("errMsg","密码错误");
                    return "admin/login/login";
                }else {
                    session.setAttribute("_admin",username);
                    return "redirect: index";
                }
            }
        }catch (Exception e){
            model.addAttribute("errMsg","服务器繁忙登录失败!");
            return "admin/login/login";
        }

    }
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute("_admin");
        return "admin/login/login";
    }
   @RequestMapping("/user/changePasswordPage")
   public String changePasswordPage(){
       return "admin/login/changePassword";
   }
   @RequestMapping("/user/changePassword")
   public String changePassword(String oldPassword,String newPassword,HttpSession session,Model model){
       String username = (String) session.getAttribute("_admin");
       User user= userService.queryUserByName(username,0);
       if (!EncryptionUtils.encryptMD5(oldPassword).equals(user.getPassword())){
            model.addAttribute("errMessage","密码错误修改失败");
            return "500";
       }else {
           user.setPassword(newPassword);
           try {
               int i = userService.updateUser(user);
               if (i==0){
                   model.addAttribute("errMessage","服务器繁忙修改失败");
                   return "500";
               }
           }catch (Exception e){
               model.addAttribute("errMessage",e.getMessage());
               return "500";
           }
           model.addAttribute("url", "admin/index");
           return "success";
       }
   }
}
