package com.eshop.controller.admin;

import com.eshop.domain.User;
import com.eshop.service.UserService;
import com.eshop.utils.PageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/admin")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/user/list")
    public String list(@RequestParam(defaultValue = "1")Integer pageNo,
                       @RequestParam(defaultValue = "10") Integer pageSize, Model model){
        PageModel<User> userPages = userService.queryUserList(pageNo, pageSize);
        model.addAttribute("userPages",userPages);
        return "admin/user/list";
    }
    @RequestMapping("/user/addPage")
    public String addPage(){
        return "admin/user/add";
    }
    @RequestMapping("/user/add")
    public String addUser(User user,Model model){
        try {
            user.setType(0);
            int i = userService.addUser(user);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙添加失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/user/list");
        return "success";

    }
    @RequestMapping("user/updatePage")
    public String updatePage(String id,Model model){
        User user = userService.queryUserById(id);
        model.addAttribute("user",user);
        return "admin/user/update";
    }
    @RequestMapping("/user/update")
    public String updateUser(User user,Model model){
        try {
            int i = userService.updateUser(user);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙修改失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/user/list");
        return "success";

    }
    @RequestMapping("user/deletePage")
    public String deletePage(String id,Model model){
        model.addAttribute("id",id);
        return "admin/user/delete";
    }
    @RequestMapping("user/delete")
    public String deleteUser(String id,Model model){
        try {
            int i = userService.deleteUser(id);
            if (i==0){
                model.addAttribute("errMessage","服务器繁忙修改失败");
                return "500";
            }
        }catch (Exception e){
            model.addAttribute("errMessage",e.getMessage());
            return "500";
        }
        model.addAttribute("url", "admin/user/list");
        return "success";
    }
}
