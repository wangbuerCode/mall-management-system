package com.eshop.service;

import com.eshop.domain.OrderProduct;

import java.util.List;

public interface OrderProductService {
    List<OrderProduct> queryOrderProByOrderId(String orderId);

    void addOrdProOne(OrderProduct orderProduct);
}
