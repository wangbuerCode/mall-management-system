package com.eshop.service;

import com.eshop.domain.Carousel;
import com.eshop.utils.PageModel;

import java.util.List;

public interface CarouselService {
    PageModel<Carousel> queryCarouselPages(Integer pageNo, Integer pageSize);

    int addCarousel(Carousel carousel);

    Carousel queryCarouselById(String id);

    int updateCarousel(Carousel carousel);

    int delete(String id);

    List<Carousel> queryCarouselAll();
}
