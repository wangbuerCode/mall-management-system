package com.eshop.service;

import com.eshop.domain.Order;

import java.util.List;

public interface OrderService {
    List<Order> queryAllOrder(String userId);

    void addOrderOne(Order order);
}
