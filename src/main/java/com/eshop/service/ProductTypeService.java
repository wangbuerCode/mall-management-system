package com.eshop.service;

import com.eshop.domain.ProductType;
import com.eshop.utils.PageModel;

import java.util.List;

public interface ProductTypeService {
    PageModel<ProductType> queryProductTypePage(Integer pageNo, Integer pageSize);

    int addProductType(ProductType productType);

    ProductType queryProductTypeById(String id);

    int updateProductType(ProductType productType);

    int deleteProductTypeById(String id);

    void batchProductTypeDel(String[] ids);

    List<ProductType> queryProductTypeAll();
}
