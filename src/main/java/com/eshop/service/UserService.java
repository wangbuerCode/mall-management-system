package com.eshop.service;

import com.eshop.domain.User;
import com.eshop.utils.PageModel;
import org.apache.ibatis.annotations.Param;


public interface UserService {
    PageModel<User> queryUserList(int pageNum, int pageSize);

    int addUser(User user);

    User queryUserById(@Param("id") String id);

    int updateUser(User user);

    int deleteUser(String id);

    User queryUserByName(String username, int type);
}
