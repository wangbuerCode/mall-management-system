package com.eshop.service;

import com.eshop.domain.Brand;
import com.eshop.utils.PageModel;

import java.util.List;

public interface BrandService {
    PageModel<Brand> queryBrandPages(Integer pageNo, Integer pageSize);

    int addBrand(Brand brand);

    Brand queryBrandById(String id);

    int updateBrand(Brand brand);

    int delete(String id);

    void batchProductTypeDel(String[] ids);

    List<Brand> queryBrandAll();

    List<Brand> queryBrandByTypeId(String type);
}
