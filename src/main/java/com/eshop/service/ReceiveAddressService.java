package com.eshop.service;

import com.eshop.domain.ReceiveAddress;

import java.util.List;

public interface ReceiveAddressService {
    List<ReceiveAddress> queryAddressByUserID(String userId);

    int addAddress(ReceiveAddress receiveAddress);

    int updateAddress(ReceiveAddress receiveAddress);

    int deleteAddressById(String id);

    int setDefault(String id,String userId);
}
