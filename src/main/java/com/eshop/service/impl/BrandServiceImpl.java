package com.eshop.service.impl;

import com.eshop.domain.Brand;
import com.eshop.mapper.BrandMapper;
import com.eshop.service.BrandService;
import com.eshop.utils.PageModel;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    private BrandMapper mapper;
    @Autowired
    public void setMapper(BrandMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public PageModel<Brand> queryBrandPages(Integer pageNo, Integer pageSize) {
        List<Brand> brands=mapper.queryBrandPages((pageNo-1)*pageSize,pageSize);
        PageModel<Brand> brandPageModel = new PageModel<Brand>();
        brandPageModel.setPageSize(pageSize);
        brandPageModel.setPageNo(pageNo);
        brandPageModel.setList(brands);
        brandPageModel.setTotalRecords(mapper.queryBrandCount());
        return brandPageModel;
    }

    @Override
    public int addBrand(Brand brand) {
        brand.setId(UUIDUtils.getId());
        return mapper.addBrand(brand);
    }

    @Override
    public Brand queryBrandById(String id) {
        return mapper.queryBrandById(id);
    }

    @Override
    public int updateBrand(Brand brand) {
        return mapper.updateBrand(brand);
    }

    @Override
    public int delete(String id) {
        return mapper.delete(id);
    }

    @Override
    public void batchProductTypeDel(String[] ids) {
        for (String id : ids) {
            mapper.delete(id);
        }
    }

    @Override
    public List<Brand> queryBrandAll() {
        return mapper.queryBrandAll();
    }

    @Override
    public List<Brand> queryBrandByTypeId(String type) {
        return mapper.queryBrandByTypeId(type);
    }
}
