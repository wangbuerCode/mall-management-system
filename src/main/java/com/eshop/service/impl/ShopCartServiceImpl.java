package com.eshop.service.impl;

import com.eshop.domain.ShopCart;
import com.eshop.mapper.ShopCartMapper;
import com.eshop.service.ShopCartService;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopCartServiceImpl implements ShopCartService {
    private ShopCartMapper mapper;
    @Autowired
    public void setMapper(ShopCartMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ShopCart queryShopCartByUserID(String userId) {
        ShopCart shopCart= mapper.queryShopCartByUserID(userId);
        if (shopCart==null){
            ShopCart shopCart1=new ShopCart();
            shopCart1.setUserId(userId);
            shopCart1.setCartId(UUIDUtils.getId());
            shopCart1.setId(UUIDUtils.getId());
            mapper.addShopCart(shopCart1);
            return shopCart1;
        }
        return shopCart;
    }


}
