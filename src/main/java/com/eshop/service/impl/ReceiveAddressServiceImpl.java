package com.eshop.service.impl;

import com.eshop.domain.ReceiveAddress;
import com.eshop.mapper.ReceiveAddressMapper;
import com.eshop.service.ReceiveAddressService;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ReceiveAddressServiceImpl implements ReceiveAddressService {
    private ReceiveAddressMapper mapper;
    @Autowired
    public void setMapper(ReceiveAddressMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<ReceiveAddress> queryAddressByUserID(String userId) {
        return mapper.queryAddressByUserID(userId);
    }

    @Override
    public int addAddress(ReceiveAddress receiveAddress) {
        receiveAddress.setId(UUIDUtils.getId());
        if (receiveAddress.getIsDefault()==1){
            mapper.updateAddressAll(receiveAddress.getUserId());
        }
        return mapper.addAddress(receiveAddress);
    }

    @Override
    public int updateAddress(ReceiveAddress receiveAddress) {
        System.out.println(receiveAddress);
        if (receiveAddress.getIsDefault()== 1 || StringUtils.isEmpty(receiveAddress.getIsDefault())){
            mapper.updateAddressAll(receiveAddress.getUserId());
        }
        return mapper.updateAddress(receiveAddress);
    }

    @Override
    public int deleteAddressById(String id) {
        return mapper.deleteAddressById(id);
    }

    @Override
    public int setDefault(String id ,String userId) {
        mapper.updateAddressAll(userId);
        return mapper.setDefault(id);
    }
}
