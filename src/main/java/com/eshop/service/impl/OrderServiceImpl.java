package com.eshop.service.impl;

import com.eshop.domain.Order;
import com.eshop.mapper.OrderMapper;
import com.eshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper mapper;

    @Override
    public List<Order> queryAllOrder(String userId) {
        return mapper.queryAllOrder(userId);
    }

    @Override
    public void addOrderOne(Order order) {
        mapper.addOrderOne(order);
    }
}
