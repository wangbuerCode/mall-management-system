package com.eshop.service.impl;

import com.eshop.domain.Carousel;
import com.eshop.mapper.CarouselMapper;
import com.eshop.service.CarouselService;
import com.eshop.utils.PageModel;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarouselServiceImpl  implements CarouselService {
    private CarouselMapper mapper;
    @Autowired
    public void setMapper(CarouselMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public PageModel<Carousel> queryCarouselPages(Integer pageNo, Integer pageSize) {
        //1.调用mapper查询轮播图信息
        List<Carousel> Carousels = mapper.queryCarouselPages((pageNo-1)*pageSize,pageSize);
        PageModel<Carousel> pages = new PageModel<Carousel>();
        //2.将轮播图信息和分页参数存进PageModel对象中
        pages.setList(Carousels);
        pages.setPageNo(pageNo);
        pages.setPageSize(pageSize);
        pages.setTotalRecords(mapper.queryCarouselCount());
        return pages;
    }

    @Override
    public int addCarousel(Carousel carousel) {
        carousel.setId(UUIDUtils.getId());
        return mapper.addCarousel(carousel);
    }

    @Override
    public Carousel queryCarouselById(String id) {
        return mapper.queryCarouselById(id);
    }

    @Override
    public int updateCarousel(Carousel carousel) {
        return mapper.updateCarousel(carousel);
    }

    @Override
    public int delete(String id) {
        return mapper.delete(id);
    }

    @Override
    public List<Carousel> queryCarouselAll() {
        return mapper.queryCarouselAll();
    }
}
