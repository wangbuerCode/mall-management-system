package com.eshop.service.impl;

import com.eshop.domain.OrderProduct;
import com.eshop.mapper.OrderProductMapper;
import com.eshop.service.OrderProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderProductServiceImpl implements OrderProductService {
    @Autowired
    private OrderProductMapper mapper;

    @Override
    public List<OrderProduct> queryOrderProByOrderId(String orderId) {
        return mapper.queryOrderProByOrderId(orderId);
    }

    @Override
    public void addOrdProOne(OrderProduct orderProduct) {
        mapper.addOrdProOne(orderProduct);
    }
}
