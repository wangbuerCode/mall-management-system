package com.eshop.service.impl;

import com.eshop.domain.ProductType;
import com.eshop.mapper.ProductTypeMapper;
import com.eshop.service.ProductTypeService;
import com.eshop.utils.PageModel;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductTypeServiceImpl implements ProductTypeService {
    private ProductTypeMapper mapper;
    @Autowired
    public void setMapper(ProductTypeMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public PageModel<ProductType> queryProductTypePage(Integer pageNo, Integer pageSize) {
        List<ProductType> list=mapper.queryProductTypePage((pageNo-1)*pageSize,pageSize);
        PageModel<ProductType> pages=new PageModel<ProductType>();
        pages.setList(list);
        pages.setPageNo(pageNo);
        pages.setPageSize(pageSize);
        pages.setTotalRecords(mapper.queryProductTypeCount());
        return pages;
    }

    @Override
    public int addProductType(ProductType productType) {
        productType.setId(UUIDUtils.getId());
        return mapper.addProductType(productType);
    }

    @Override
    public ProductType queryProductTypeById(String id) {
        return mapper.queryProductTypeById(id);
    }

    @Override
    public int updateProductType(ProductType productType) {
        return mapper.updateProductType(productType);
    }

    @Override
    public int deleteProductTypeById(String id) {
        return mapper.deleteProductTypeById(id);
    }

    @Override
    public void batchProductTypeDel(String[] ids) {
        for (String id : ids) {
            mapper.deleteProductTypeById(id);
        }
    }

    @Override
    public List<ProductType> queryProductTypeAll() {
        return mapper.queryProductTypeAll();
    }
}
