package com.eshop.service.impl;

import com.eshop.domain.SearchHistory;
import com.eshop.mapper.SearchHistoryMapper;
import com.eshop.service.SearchHistoryService;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SearchHistoryServiceImpl implements SearchHistoryService {
    private SearchHistoryMapper mapper;
    @Autowired
    public void setMapper(SearchHistoryMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<SearchHistory> querySearchHistoryPages(int num) {
        return mapper.querySearchHistoryPages(num);
    }

    @Override
    public void addUpdateWords(String words) {
        boolean exist = mapper.isExist(words);
        System.out.println("是否存在："+exist);
        if (exist) {//存在，就修改搜索次数
            mapper.updateSearchHistory(words);
        }else {//不存在，就新增一个搜索词
            SearchHistory searchHistory = new SearchHistory();
            searchHistory.setId(UUIDUtils.getId());
            searchHistory.setNum(1);
            searchHistory.setSearchTime(new Date());
            searchHistory.setSearchWords(words);
            mapper.addSearchHistory(searchHistory);
        }
    }
}
