package com.eshop.service.impl;

import com.eshop.domain.User;
import com.eshop.mapper.UserMapper;
import com.eshop.service.UserService;
import com.eshop.utils.EncryptionUtils;
import com.eshop.utils.PageModel;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userS")
public class UserServiceImpl implements UserService {
    private UserMapper userMapper;
    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public PageModel<User> queryUserList(int pageNum, int pageSize) {
        List<User> users = userMapper.queryUserList((pageNum - 1) * pageSize, pageSize);
        PageModel<User> userPageModel=new PageModel<User>();
        userPageModel.setList(users);
        userPageModel.setPageNo(pageNum);
        userPageModel.setPageSize(pageSize);
        userPageModel.setTotalRecords(userMapper.queryUserCount());
        return userPageModel;
    }

    @Override
    public int addUser(User user) {
        user.setId(UUIDUtils.getId());
        user.setPassword(EncryptionUtils.encryptMD5(user.getPassword()));
        return userMapper.addUser(user);
    }

    @Override
    public User queryUserById(String id) {
        return userMapper.queryUserById(id);
    }

    @Override
    public int updateUser(User user) {
        user.setPassword(EncryptionUtils.encryptMD5(user.getPassword()));
        return userMapper.updateUser(user);
    }

    @Override
    public int deleteUser(String id) {
        return userMapper.deleteUser(id);
    }

    @Override
    public User queryUserByName(String username, int type) {
        return userMapper.queryUserByName(username,type);
    }
}
