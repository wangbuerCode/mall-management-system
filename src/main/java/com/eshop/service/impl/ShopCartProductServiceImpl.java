package com.eshop.service.impl;

import com.eshop.domain.Product;
import com.eshop.domain.ShopCart;
import com.eshop.domain.ShopCartProduct;
import com.eshop.mapper.ShopCartMapper;
import com.eshop.mapper.ShopCartProductMapper;
import com.eshop.service.ShopCartProductService;
import com.eshop.utils.UUIDUtils;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopCartProductServiceImpl implements ShopCartProductService {
    @Autowired
    private ShopCartProductMapper mapper;


    @Override
    public List<ShopCartProduct> queryCartProductAll(String cartId) {
        return mapper.queryCartProductAll(cartId);
    }

    @Override
    public void addShop(ShopCartProduct shopCartProduct) {
        int i=mapper.queryShopCartByProductID(shopCartProduct.getProduct().getId());
        if(i == 0){
            shopCartProduct.setId(UUIDUtils.getId());
            mapper.addShop(shopCartProduct);
        }else {
            mapper.update(shopCartProduct);
        }

    }

    @Override
    public int deleteById(String id) {
        return mapper.deleteById(id);
    }

    @Override
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            mapper.deleteById(id);
        }
    }

    @Override
    public void deleteShopCartBy(String cartId, String proId) {
        mapper.deleteShopCartBy(cartId,proId);
    }
}
