package com.eshop.service.impl;


import com.eshop.domain.Product;
import com.eshop.mapper.ProductMapper;
import com.eshop.service.ProductService;
import com.eshop.utils.EncryptionUtils;
import com.eshop.utils.PageModel;
import com.eshop.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductMapper mapper;
    @Autowired
    public void setMapper(ProductMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public PageModel<Product> queryProductPage(Integer pageNo, Integer pageSize, String productName, String productTypeID) {
        List<Product> products=mapper.queryProductPage((pageNo-1)*pageSize,pageSize,productName,productTypeID);
        PageModel<Product> productPageModel=new PageModel<Product>();
        productPageModel.setList(products);
        productPageModel.setPageNo(pageNo);
        productPageModel.setPageSize(pageSize);
        productPageModel.setTotalRecords(mapper.queryProductCount(productName,productTypeID));
        return productPageModel;
    }

    @Override
    public int addProduct(Product product) {
        product.setCreateTime(new Date());
        product.setId(UUIDUtils.getId());
        return mapper.addProduct(product);
    }

    @Override
    public Product queryProductById(String id) {
        return mapper.queryProductById(id);
    }

    @Override
    public int updateProduct(Product product) {
        return mapper.updateProduct(product);
    }

    @Override
    public int delete(String id) {
        return mapper.delete(id);
    }

    @Override
    public void batchProductTypeDel(String[] ids) {
        for (String id : ids) {
            mapper.delete(id);
        }
    }

    @Override
    public List<Product> queryNewProduct(int num) {
        return mapper.queryNewProduct(num);
    }

    @Override
    public List<Product> queryProductsByType(Product product, int num) {
        return mapper.queryProductsByType(product,num);
    }

    @Override
    public List<Product> queryProductRankings() {
        return mapper.queryProductRankings();
    }

    @Override
    public int querySalesByProductId(String id) {
        return mapper.querySalesByProductId(id);
    }
}
