package com.eshop.service;

import com.eshop.domain.ShopCart;
import com.eshop.domain.ShopCartProduct;

import java.util.List;

public interface ShopCartProductService {
    List<ShopCartProduct> queryCartProductAll(String cartId);

    void addShop(ShopCartProduct shopCartProduct);

    int deleteById(String id);

    void deleteAll(String[] ids);

    void deleteShopCartBy(String cartId, String proId);
}
