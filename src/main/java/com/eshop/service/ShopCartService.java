package com.eshop.service;

import com.eshop.domain.ShopCart;

public interface ShopCartService {
    ShopCart queryShopCartByUserID(String UserId);

}
