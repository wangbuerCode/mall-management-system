package com.eshop.service;

import com.eshop.domain.Product;
import com.eshop.utils.PageModel;

import java.util.List;

public interface ProductService {
    PageModel<Product> queryProductPage(Integer pageNo, Integer pageSize, String productName, String productTypeID);

    int addProduct(Product product);

    Product queryProductById(String id);

    int updateProduct(Product product);

    int delete(String id);

    void batchProductTypeDel(String[] ids);

    List<Product> queryNewProduct(int num);

    List<Product> queryProductsByType(Product product, int num);

    List<Product> queryProductRankings();

    int querySalesByProductId(String id);
}
