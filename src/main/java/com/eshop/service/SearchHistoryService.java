package com.eshop.service;

import com.eshop.domain.SearchHistory;

import java.util.List;

public interface SearchHistoryService {
    List<SearchHistory> querySearchHistoryPages(int num);

    void addUpdateWords(String words);
}
