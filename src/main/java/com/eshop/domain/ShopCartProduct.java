package com.eshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShopCartProduct {
    private String id;
    private ShopCart shopCart;
    private Product product;
    private int productNum;
}
