package com.eshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductType {
    private String id;
    private String productTypeName;
    private String productTypeDesc;
    private String productTypeIcon;
}
