package com.eshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveAddress {
    private String id;
    private String receivingAddress;
    private String receivingPerson;
    private Long mobilePhone;
    private String userId;
    private int isDefault;

}
