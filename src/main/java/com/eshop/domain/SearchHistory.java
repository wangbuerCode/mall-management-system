package com.eshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchHistory {
    private String id;
    private String searchWords;
    private int num;
    private Date searchTime;
}
