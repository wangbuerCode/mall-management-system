package com.eshop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String id;
    private Date CreateTime;
    private ReceiveAddress receiveAddress;
    private String userId;
    private List<OrderProduct> list;
}
