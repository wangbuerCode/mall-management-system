package com.eshop.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private String id;
    private String productName;
    private String productImage;
    private Double price;
    private ProductType productType;
    private String productDesc;
    private Date createTime;
    private Brand productBrand;
    private int sales;

}
