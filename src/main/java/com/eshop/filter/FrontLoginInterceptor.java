package com.eshop.filter;

import com.eshop.domain.User;
import com.eshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FrontLoginInterceptor implements HandlerInterceptor {
    @Autowired
    private UserService userService;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // TODO Auto-generated method stub
        Cookie[] cookies = request.getCookies();
        User user = new User();
        if(cookies!=null){
            for (Cookie c:cookies){
                if(c.getName().equals("username")){
                    String username = c.getValue();
                    user= userService.queryUserByName(username,1);
                }
            }
        }
        Object user1 = request.getSession().getAttribute("user");
        if(user1==null&&user.getUsername()!=null){
            request.getSession().setAttribute("user",user);
        }
       return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        // TODO Auto-generated method stub

    }
}


