package com.eshop.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/admin/*")
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig config) throws ServletException {

    }
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request= (HttpServletRequest) req;
        HttpServletResponse response= (HttpServletResponse) resp;
        HttpSession session = request.getSession();
        Object username = session.getAttribute("_admin");
        if (username==null){
            String servletPath = request.getServletPath();
            if (servletPath.equals("/admin/loginPage")||servletPath.equals("/admin/login")){
                chain.doFilter(req, resp);
            }else {
                if (isAjaxRequest(request)) {//异步请求
                    response.setContentType("application/json;charset=utf-8");
                    response.getWriter().write("{'code':'403','msg':'异步请求,访问被拒绝,客户端未授权!'}");
                    return;
                }else{//同步请求
                    response.setCharacterEncoding("utf-8");
                    response.sendRedirect(request.getContextPath()+"/admin/loginPage");
                    return;
                }
            }
        }else {
            chain.doFilter(req, resp);
        }

    }
    @Override
    public void destroy() {
    }
    public boolean isAjaxRequest(HttpServletRequest request){
        String header = request.getHeader("X-Requested-With");
        if (header != null && header.length() > 0) {
            if ("XMLHttpRequest".equalsIgnoreCase(header))
                return true;
        }
        return false;
    }

}
